$(document).ready(function() {
    $("#success").fadeOut(5000);

    $('select.product').change(function() {

        var other_val = $('select.product option[value="' + $(this).val() + '"]').data('value');
        $('#itGlueConfTypeName').val(other_val);

    });

    $('select.type').change(function() {

        var other_val = $('select.type option[value="' + $(this).val() + '"]').data('value');
        $('#product_name').val(other_val);

    });
    document.documentElement.scrollTop = sessionStorage.scrollPosition
    console.log( sessionStorage.scrollPosition );
    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        console.log( scroll );
        sessionStorage.scrollPosition = scroll
    });
});