<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->unsignedInteger('itGlueId')->primary();
            $table->string('itGlueStatus');
            $table->unsignedInteger('itGlueCustomerId');
            $table->string('product_id');
            $table->string('itGlueName');
            $table->string('itGlueType');
            $table->string('itGlueTypeId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
