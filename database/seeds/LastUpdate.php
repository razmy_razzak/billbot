<?php

use Illuminate\Database\Seeder;

class LastUpdate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SyncyInfo::create([
            'type' =>'lastUpdate',
            'time' =>'000-00-00 00:00:00',
        ]);

        \App\SyncyInfo::create([
            'type' =>'configUpdate',
            'time' =>'000-00-00 00:00:00',
        ]);

        \App\SyncyInfo::create([
            'type' =>'synced',
            'time' => time(),
        ]);
    }
}
