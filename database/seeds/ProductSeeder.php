<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Products::create([
            'name' =>'Computer',
            'description' =>'Computer',
            'product_type' =>\App\Objects\ConstValues::SPECIAL_PRODUCT,
            'deletable' =>1,
            'price' =>20,
        ]);

        \App\Products::create([
            'name' =>'Device',
            'description' =>'Device',
            'deletable' =>1,
            'product_type' =>\App\Objects\ConstValues::SPECIAL_PRODUCT,
            'price' =>20,
        ]);

    }
}
