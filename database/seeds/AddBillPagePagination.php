<?php

use Illuminate\Database\Seeder;

class AddBillPagePagination extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Settings::create([
            'field_name' =>\App\Objects\ConstValues::BILL_PAGE_PAGINATION,
            'field_value' =>100,
        ]);
    }
}
