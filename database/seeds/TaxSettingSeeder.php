<?php

use Illuminate\Database\Seeder;

class TaxSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Settings::create([
            'field_name' =>\App\Objects\ConstValues::TAX_BILL_SETTINGS,
            'field_value' =>'0',
        ]);
    }
}
