<?php

use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' =>'SuperAdmin',
            'email' => 'admin@gmail.com',
            'password' => 'admin'
        ]);
    }
}
