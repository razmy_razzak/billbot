<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SuperAdminSeeder::class);
        $this->call(LastUpdate::class);
        $this->call(ProductSeeder::class);
        $this->call(TaxSettingSeeder::class);
        $this->call(AddBillPagePagination::class);
    }
}
