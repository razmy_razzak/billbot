# ImaraSoft - Billbot

## Installation guide ss
* **Required steps**
   * clone repository 
   * run composer install 
   * create .env file and update the email, database and ITGLUE API details 
   * run php artisan db:seed ( to create super admin and last update time -u=admin@gmail.com -p=admin)
   
* **Custom command to testAPI sync**
    *php artisan  ITGlue:updateCustomer ( this will print api Synced infor)

## Developer Details   
* **Developer**: RazmyRazzak
* **Latest Stable**: Master
* **Web**: [http://165.227.13.25](https://example.com/)




## Client Details 

* **Client Name**: Tyler
* **Client Company**: Hansen Greess
* **Client Source**: Freelancing.com
* **Client email**: example@gmail.com
* **Client Skype**: Skype ID
* **Client Phone**: 012345


## Software Description 
We run an IT service company and we bill a monthly flat rate based on devices and computers at our clients. We use software called IT Glue to document and track this infrastructure. We need a database created that tracks the devices and computers that we are actually billing for (along with a few other manual billing items). This database will be populated by querying the IT Glue API, and then allow our billing staff to review the changes before updating the invoice amounts stored in the database.
https://docs.google.com/document/d/1BMy-rOggQnB49ntg-8l7UDKLJ8HMkGn8yMo9LN07P3Q/edit?usp=sharing

