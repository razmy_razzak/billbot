<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 25/07/2018
 * Time: 11:29
 */

namespace App\Services;

use App\LineItems;
use App\Objects\ConfigurationClass;
use App\Objects\ConstValues;
use App\Objects\CustomerClass;
use App\Objects\SyncedClass;
use App\Repository\ConfigRepository;
use App\Repository\CustomerRepository;
use App\Repository\ItemTypeRepository;
use App\Repository\LineItemRepository;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use App\Services\ItGlueServices;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Support\Facades\Log;


class CustomerService
{
    private $itGlueServices;
    private $customerRepo;
    private $configRepo;
    private $productRepo;
    private $lineItemRepo;
    private $settingsRepo;


    public function __construct(
        ItGlueServices $itGlueServices,
        CustomerRepository $customerRepository,
        ConfigRepository $configRepository,
        ProductRepository $productRepository,
        LineItemRepository $lineItemRepository,
        SettingRepository $settingRepository

    )
    {
        $this->itGlueServices = $itGlueServices;
        $this->customerRepo = $customerRepository;
        $this->configRepo = $configRepository;
        $this->productRepo = $productRepository;
        $this->lineItemRepo = $lineItemRepository;
        $this->settingsRepo = $settingRepository;
    }

    public function updateOrCreateCustomerData(){
        $page = 1;
        $updated =0;
        $update_array=[];
        $created =0;
        $created_array=[];
        $current_total_customers= $this->customerRepo->countRow();
        $current_total_configs= $this->configRepo->countRow();
        $page_count = 0;

        for( $i =1; $i<=$page; $i++){
            $itGlue_customersNew = $this->itGlueServices->getCustomerForUpdateOrCreate( $i );
            /** @var CustomerClass $customer */
            foreach ( $itGlue_customersNew['cutomers'] as $customer ){
                $find_customer = $this->customerRepo->customFind( 'itGlueId', $customer->getItGlueId() );
                $data = [
                    'itGlueId' => $customer->getItGlueId(),
                    'tax' => $customer->getTax(),
                    'itGlueName' => $customer->getItGlueName(),
                    'itGlueUpdated' => $customer->getItGlueUpdated(),
                    'itGlueStatus' => $customer->getItGlueStatus()
                ];
                if( $find_customer ){
                    //this is to update
                    $this->customerRepo->update( $find_customer->itGlueId, $data );
                    $updated ++;
                    $update_array[]=$customer->getItGlueName();
                }
                else{
                    //this is to create
                    $delta = $this->customerRepo->create( $data );
                    $created ++;
                    $created_array[]=$delta->itGlueName;
                }
            }
            $page =$itGlue_customersNew['nextPage'];
            $page_count = $i;
        }

        $conf_coint = $this->getConfiguration();
        $removed_data = $this->deleteConfigItems();
        $conf_creatd =[];
        $conf_modifyd =[];
        $updated_count = 0;
        $created_count =0;
        $page_count_conf =0;
        if( $conf_coint ){
            $created_count = $conf_coint['conf_created'];
            $updated_count = $conf_coint['conf_update'];
            $conf_creatd = $conf_coint['conf_created_array'];
            $conf_modifyd = $conf_coint['conf_updated_array'];
            $page_count_conf = $conf_coint['page_count'];
        }

        $customer_data =[
            'Unchanged' => ($current_total_customers - $updated ),
            'New' => $created,
            'Changed' => $updated,
            'created_list'=> $created_array,
            'removed_customers'=> $removed_data['removed_customers'],
            'updated_list' => $update_array,
            'page' => $page_count
        ];

        $config_data =[
            'Unchanged' => ($current_total_configs - $updated_count ),
            'New' => $created_count,
            'Changed' => $updated_count,
            'Removed' => $removed_data['removed_configs'],
            'created_list' => $conf_creatd,
            'updated_list' =>$conf_modifyd,
            'page' => $page_count_conf
        ];

        $data = [
            'config_data' => $config_data,
            'customer_data' => $customer_data,
        ];
        return $data;
    }

    public function getConfiguration( $page =1 ){
        $page_count =0;
        $conf_update =0;
        $conf_created =0;
        $conf_updated_array =[];
        $conf_created_array =[];
        $tes =['update_at'];
        for( $i =1; $i<=$page; $i++){
            $itGlue_customersNew = $this->itGlueServices->getConfiguration( $i );
            /** @var ConfigurationClass $configuration */
            foreach ( $itGlue_customersNew['configurations'] as $configuration ){
                $find_config = $this->configRepo->find( $configuration->getItGlueId() );
                //make array for database creation
                $data = [
                    'itGlueId' => $configuration->getItGlueId(),
                    'itGlueName' => $configuration->getItGlueName(),
                    'itGlueCustomerId' => $configuration->getItGlueCustomerId(),
                    'itGlueStatus' => $configuration->getItGlueStatus(),
                    'itGlueType' => $configuration->getItGlueType(),
                    'itGlueTypeId' => $configuration->getItGlueTypeId(),
                    'product_id' => $configuration->getProductId(),
                ];
                if( $find_config ){
                    $this->configRepo->update( $find_config->itGlueId , $data );
                    $conf_update++;
                    $conf_updated_array[] = $configuration->getItGlueName();
                }
                else{
                    // create configs only if organization or customer exist
                    $find_customer = $this->customerRepo->customFind( 'itGlueId', $configuration->getItGlueCustomerId() );
                    if( $find_customer ){
                        $this->configRepo->create( $data );
                        $conf_created ++;
                        $conf_created_array[]= $configuration->getItGlueName();
                    }
                }
            }
            $page =$itGlue_customersNew['nextPage'];
            $page_count = $i;
        }
        return ['conf_created' => $conf_created, 'conf_update'=> $conf_update, 'conf_created_array'=> $conf_created_array, 'conf_updated_array'=>$conf_updated_array,'page_count' => $page_count ];
    }


    public function getCustomerSyncedData(){
        try{
            $objectArray =[];
            $pagination_defualt = 100;
            $pagination = 0;
            $pagination_setings = $this->settingsRepo->customFind('field_name' , ConstValues::BILL_PAGE_PAGINATION);
            if( $pagination_setings ){
                $pagination = $pagination_setings->field_value;
                $pagination = round( (float)$pagination );
            }
            $pagination = $pagination == 0 ? $pagination_defualt : $pagination;
            $collections = $this->customerRepo->getAllCustomersWithConfigs( $pagination );
            $products = $this->productRepo->getProductsByTypes('product_type', ConstValues::SPECIAL_PRODUCT ,ConstValues::NORMAL_PRODUCT);

            foreach ( $collections as $data ){
                $data = new SyncedClass( $data ,$products , $this->lineItemRepo );
                $objectArray[] = $data;
            }
            $result = [
                'objectArray' =>  $objectArray,
                'pagination' => $collections->links()
            ];
            return $result;
        }
        catch (\Exception $e ){
            Log::error('sync Error' . $e->getMessage() );
            abort(404, $e->getMessage());
        }

    }

    public function getCustomerWithLineItems( $customer_id ){
        //customer + lineitem from last synced data
        $customer = $this->customerRepo->getCustomersWithLineItems( $customer_id );

        // latest config ids from recent synced data
        $configs = $this->configRepo->getConfigsIdsForCustomerId( $customer_id );
        /**
         * @var  $key
         * @var LineItems $lineItem
         */
        foreach ( $customer->lineItems as $key => $lineItem ){
            $item_data= [];
            $configs_names = explode(',', $lineItem['detail']);
            foreach ($configs_names as $name){
                $id = trim(preg_replace("/[^0-9\.]/", " ", $name));
                in_array($id, (array)$configs )? $icon ='fa-check bluetick' : $icon ='fa-remove redtick';
                if($icon == 'fa-remove redtick'){

                }
                $item_data[]=[
                    'id' => $id,
                    'icon' => $icon,
                    'name' => $name
                ];
                $lineItem->setAttribute('configs',$item_data);
            }
        }
        return $customer;
    }

    /*
     * this script to run after all update done with api, we need a way to delete config
     */
    public function deleteConfigItems(){
        $customers_ids = $this->customerRepo->getAllCustomersIds();
        $confid_ids = [];
        $page = 1;
        $page_count =0;

        $api_config_array=[];
        $config_array=[];
        $removed_configs = 0;
        $removed_customers = 0;

        if( $customers_ids ){
            for( $i =1; $i<=$page; $i++) {
                $api_configs='';
                foreach ($customers_ids as $customer_id => $customer_name) {
                    $result = $this->itGlueServices->getCustomerByID( $customer_id );
                    //if result is false meaning no customer found in the api
                    // so simply delete the customer, it's congigs and Line items
                    if( $result ){

                        //get all configs for this customer
                        $api_configs = $this->itGlueServices->getConfigurationForOrg( $customer_id );
                        $configs = $this->configRepo->getAllConfigForCustomerId( $customer_id );
                        /** @var ConfigurationClass $api_config */
                        foreach ( $api_configs['configurations'] as $api_config){
                            $api_config_array[]= $api_config->getItGlueId();
                        }
                        foreach ($configs as $config){
                            if( in_array( $config->itGlueId, $api_config_array)){
                                //do nothing
                            }
                            else{
                                //database record of config not in the api so we need to delete this
                                $this->configRepo->destroy( $config->itGlueId );
                                $removed_configs ++;
                            }
                        }
                    }
                    else{
                        // delete all the customer and it's reference
                        $this->customerRepo->destroy(  $customer_id );
                        $removed_customers ++;
                        $configs = $this->configRepo->getAllConfigForCustomerId( $customer_id );
                        $lineItems = $this->lineItemRepo->getAllLineItemByCustomerId( $customer_id );
                        if( $configs ){
                            foreach ( $configs as $config ){
                                $this->configRepo->destroy( $config->itGlueId );
                                $removed_configs ++;
                            }
                        }
                        if( $lineItems ){
                            foreach ( $lineItems as $lineItem ){
                                $this->lineItemRepo->destroy( $lineItem->id );
                            }
                        }
                    }
                }
                if($api_configs){
                    $page =$api_configs['nextPage'];
                    $page_count = $i;
                }
            }
        }
        $data=[
            'removed_configs' => $removed_configs,
            'removed_customers' => $removed_customers
        ];
        return $data;
    }


}