<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 17:08
 */

namespace App\Services;


use App\Objects\ConfigurationClass;
use App\Objects\CustomerClass;
use App\Repository\ItemTypeRepository;
use App\Repository\SyncInfoRepository;
use Illuminate\Support\Facades\Log;

class ItGlueServices
{
    private $syncInfoRepo;
    private $itemTypeRepo;

    public function __construct(
        SyncInfoRepository $syncInfoRepository,
        ItemTypeRepository $itemTypeRepository
    )
    {
        $this->syncInfoRepo = $syncInfoRepository;
        $this->itemTypeRepo = $itemTypeRepository;
    }

    public function getCustomerForUpdateOrCreate(  $page=1 ){
        $response = GuzzleHttpService::processCall('/organizations?sort=-updated_at&page[number]='.$page, 'GET', null, env('API_key'));
        $data = json_decode((string)$response->getBody());
        $last_update = $this->syncInfoRepo->customFind('type','lastUpdate');
        //update synced times
        $this->syncInfoRepo->updateByCustomField('type','synced' ,['time' => time()]);

        $last_update_time = new \DateTime( $last_update->time );
        $array =[];
        $current_page = $data->meta->{'current-page'};
        $next_page = $data->meta->{'next-page'};

        foreach ($data->data as $t){
            $customer = new CustomerClass( $t );
            if( $customer->getItGlueType() == 'Customer' ){
                if( $last_update_time < $customer->getItGlueUpdated() ){
                    // we have new or update products in the api
                    $array[]=$customer;
                }
                else{
                    //the loop must break or skip to next page
                    $next_page = null;
                    break;
                    //once this loop brakes mean no more update needed coz we already found the latest and last product in this page
                    //even if we have number of next pages
                }
            }
        }
        //update the last time with last customer
        /** @var CustomerClass $last_customer */
        if(count($array) > 0){
            $last_customer = reset($array);
            $this->syncInfoRepo->update($last_update->id,['time'=> $last_customer->getItGlueUpdated()]);
        }
        $data=[
            'cutomers' => $array,
            'nextPage' => $next_page,
            'currentPage' => $current_page
        ];
        return $data;
    }

    public function getConfigurationForOrg( $org_id,  $page=1 ){
        $response = GuzzleHttpService::processCall('/organizations/'.$org_id.'/relationships/configurations?page[number]='.$page, 'GET', null, env('API_key'));
        $data = json_decode((string)$response->getBody());
        $array =[];
        $current_page = $data->meta->{'current-page'};
        $next_page = $data->meta->{'next-page'};
        foreach ( $data->data as $t ){
            $configuration = new ConfigurationClass( $t ,$this->itemTypeRepo );
            $array[]= $configuration;
        }
        $data=[
            'configurations' => $array,
            'nextPage' => $next_page,
            'currentPage' => $current_page
        ];
        return $data;
    }

    public function getConfiguration( $page = 1, $check_last_update = false ){
        $response = GuzzleHttpService::processCall('/configurations?sort=-updated_at&page[number]='.$page, 'GET', null, env('API_key'));
        $data = json_decode((string)$response->getBody());
        $last_update = $this->syncInfoRepo->customFind('type','configUpdate');
        $last_update_time = new \DateTime( $last_update->time );

        $array =[];
        $current_page = $data->meta->{'current-page'};
        $next_page = $data->meta->{'next-page'};
        foreach ( $data->data as $t ){
            $api_time = new \DateTime( $t->attributes->{'updated-at'} );
            $configuration = new ConfigurationClass( $t ,$this->itemTypeRepo );
            if( $check_last_update ){
                $array[]=$configuration;
            }
            else{
                if( $last_update_time < $api_time ){
                    // we have new or update products in the api
                    $array[]=$configuration;
                }
                else{
                    //the loop must break or skip to next page
                    $next_page = null;
                    break;
                    //once this loop brakes mean no more update needed coz we already found the latest and last product in this page
                    //even if we have number of next pages
                }
            }


        }
        if(count($array) > 0){
            $last_customer = reset($array);
            $Tt = $last_customer->getUpdateAt();
            $this->syncInfoRepo->update($last_update->id,['time'=> $last_customer->getUpdateAt()]);
        }
        $data=[
            'configurations' => $array,
            'nextPage' => $next_page,
            'currentPage' => $current_page
        ];
        return $data;
    }

    public function getConfigurationType( $page =1 ){
        $response = GuzzleHttpService::processCall('/configuration_types?sort=-updated_at&page[number]='.$page, 'GET', null, env('API_key'));
        $data = json_decode((string)$response->getBody());
        if( isset($data->data) ){
            foreach ($data->data as $key => $item){
                $type = $this->itemTypeRepo->customFind('itGlueConfTypeName',$item->attributes->{'name'});
                if( $type ){
                    unset($data->data[$key]);
                }
            }
            $data = [
                'data' => $data->data,
                'nextPage' => $data->meta->{'next-page'},
                'currentPage' =>  $data->meta->{'current-page'}
            ];
            return $data;
        }
        if (isset($data->errors)){
            return false;
        }
    }

    public function getCustomerByID( $customer_id ){
        $response = GuzzleHttpService::processCall('/organizations/'.$customer_id , 'GET', null, env('API_key'));
        $data = json_decode((string)$response->getBody());
        if( isset($data->data) ){
            return $data->data;
        }
        if (isset($data->errors)){
            return false;
        }
    }
}