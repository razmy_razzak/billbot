<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 31/07/2018
 * Time: 15:30
 */

namespace App\Services;


use App\Repository\ConfigRepository;
use App\Repository\ItemTypeRepository;

class SyncGlueServices
{
    private $configRepo;
    private $itemTypeRepo;

    public function __construct(
        ConfigRepository $configRepository,
        ItemTypeRepository $itemTypeRepository
    )
    {
        $this->configRepo = $configRepository;
        $this->itemTypeRepo = $itemTypeRepository;
    }

    public function reSync(){
        $configs = $this->configRepo->all();
        foreach ( $configs as $config){
            $itemType = $this->itemTypeRepo->customFind('itGlueConfTypeId' , $config->itGlueTypeId );
            if( $itemType ){
                $data= [
                    'product_id' => $itemType->product_id
                ];
                $this->configRepo->update( $config->itGlueId, $data );
            }else{
                $data= [
                    'product_id' => 0
                ];
                $this->configRepo->update( $config->itGlueId, $data );
            }
        }
    }

}