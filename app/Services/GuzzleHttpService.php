<?php

/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 16:54
 */

namespace App\Services;
class GuzzleHttpService
{
    public static function processCall( $url, $method, $form_parms = null, $access_token = null ){
        switch ( $method ){
            case 'GET':
                return self::getClientService( $url , $form_parms ,$access_token );
                break;
            case 'POST':
                return self::postClientService($url, $form_parms, $access_token );
                break;
            default: return false;
        }
    }

    public static function postClientService( $url, $form_parms, $access_token =null ){
        $client = new \GuzzleHttp\Client();
        try{
            $response = $client->post(env('API_URL') . $url, [
                'headers' => [
                    'x-api-key' => $access_token,
                    'Content-Type' =>'application/vnd.api+json',
                ],
                'form_params' => $form_parms
            ] );
            return $response;
        }
        catch (\GuzzleHttp\Exception\BadResponseException $e){
            return $e->getResponse();
        }
        catch (\Exception $e){
            abort(404, 'API connection failed please try again later: error- '.$e->getMessage());
        }
    }

    public static function getClientService( $url, $form_parms= null, $access_token = null ){
        $client = new \GuzzleHttp\Client();
        try{
            $response = $client->get( env('API_URL').$url, [
                'headers' => [
                    'x-api-key' => $access_token,
                    'Content-Type' =>'application/vnd.api+json',
                ],
                'form_params' => $form_parms
            ] );
            return $response;

        }
        catch (\GuzzleHttp\Exception\BadResponseException $e){
            return $e->getResponse();
        }
        catch (\Exception $e){
            abort(404, 'API connection failed please try again later: error- '.$e->getMessage());
        }

    }
}