<?php

namespace App\Http\Controllers;

use App\Objects\ConstValues;
use App\Repository\CustomerRepository;
use App\Repository\LineItemRepository;
use App\Repository\ProductRepository;
use Illuminate\Http\Request;

class LineItemController extends Controller
{
    private $productRepo;
    private $customerRepo;
    private $lineItemRepo;

    public function __construct(
        ProductRepository $productRepository,
        CustomerRepository $customerRepository,
        LineItemRepository $lineItemRepository
    )
    {
        $this->productRepo = $productRepository;
        $this->customerRepo = $customerRepository;
        $this->lineItemRepo = $lineItemRepository;
    }

    public function index(){
        $lineItems = $this->lineItemRepo->getAllItemLineWithRelation();
        return view('pages.lineItem_index')->with( 'lineItems', $lineItems );

    }

    public function showLineItemForm( $id=null ){
        $products = $this->productRepo->getProductsForLineItems();
        $customers = $this->customerRepo->getAllCustomersIds();
        if( $id ){
            $lineItem = $this->lineItemRepo->findItemLineWithCustomerById( $id );
            return view('pages.lineItemEdit_form')->with('products', $products)->with('customers',$customers)->with('lineItem', $lineItem);
        }
        else{
            return view('pages.lineItem_form')->with('products', $products)->with('customers',$customers);
        }
    }

    public function createLineItem(Request $request){
        $this->validate( $request,[
            'qty' =>'required|numeric',
            'detail' => 'required'
        ]);
        $this->lineItemRepo->create( $request->all());
        return redirect()->back()->with('status', 'Created!');
    }

    public function updateLineItem( Request $request ){
        $this->validate( $request,[
            'qty' =>'required|numeric',
            'detail' => 'required'
        ]);
        $this->lineItemRepo->update( $request->input('id'),$request->all());
        return redirect()->back()->with('status', 'Updated!');
    }

    public function deleteLineItem( $id ){
        if($id){
            $this->lineItemRepo->destroy( $id );
            return redirect()->back()->with('status', 'Deleted!');
        }
    }

}
