<?php

namespace App\Http\Controllers;

use App\Objects\ConstValues;
use App\RepoInterfaces\ProductInterface;
use App\Repository\ConfigRepository;
use App\Repository\ItemTypeRepository;
use App\Repository\LineItemRepository;
use App\Repository\ProductRepository;
use App\Repository\SettingRepository;
use App\Repository\SyncInfoRepository;
use App\Services\CustomerService;
use App\Services\ItGlueServices;
use App\Services\SyncGlueServices;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepo;
    private $itemTypeRepo;
    private $itGlueServices;
    private $configRepo;
    private $customerServices;
    private $syncyInforRepo;
    private $lineItemRespo;
    private $settingsRepo;
    private $syncGlueServices;

    public function __construct(
        ProductRepository $productRepository,
        ItemTypeRepository $itemTypeRepository,
        ItGlueServices $itGlueServices,
        ConfigRepository $configRepository,
        CustomerService $customerService,
        SyncInfoRepository $syncInfoRepository,
        LineItemRepository $lineItemRepository,
        SettingRepository $settingRepository,
        SyncGlueServices $syncGlueServices
    )
    {
        $this->productRepo = $productRepository;
        $this->itemTypeRepo = $itemTypeRepository;
        $this->itGlueServices = $itGlueServices;
        $this->configRepo = $configRepository;
        $this->customerServices = $customerService;
        $this->syncyInforRepo = $syncInfoRepository;
        $this->lineItemRespo = $lineItemRepository;
        $this->settingsRepo = $settingRepository;
        $this->syncGlueServices = $syncGlueServices;
    }

    public function index(){
        $products = $this->productRepo->all();
        return view('pages.product_index')->with( 'products' , $products );
    }

    public function showForm( $id=null ){
        if($id){
            $product = $this->productRepo->find( $id );
            return view('pages.product_edit')->with( 'product', $product );
        }
        else{
            return view('pages.product_create');
        }
    }

    public function doEdit( Request $request ){
        $this->validate( $request,[
            'price' => 'required'
        ]);
        $this->productRepo->update( $request->input('product_id'), $request->except('product_id') );
        return redirect()->back()->with('status', 'Updated!');
    }

    public function doCreate( Request $request ){
        $this->validate( $request,[
            'name' => 'required|unique:products,name',
            'price' => 'required'
        ]);

        $this->productRepo->create( $request->all() );
        return redirect()->back()->with('status', 'Created!');
    }


    public function showType(){
        $itemTypes = $this->itemTypeRepo->getTypesByAlpha();
        return view('pages.itemType_index')->with( 'itemTypes', $itemTypes );
    }

    public function showTypeForm( $id=null){
        $products = $this->productRepo->listOfProducts();
        $itGlueTypes = $this->itGlueServices->getConfigurationType();
        if($id){
            $types = $this->itemTypeRepo->find( $id );
            return view('pages.type_edit')->with( 'products', $products )->with( 'types' , $types )->with('itGlueTypes', $itGlueTypes['data']);
        }
        else{
            return view('pages.type_create')->with( 'products', $products )->with('itGlueTypes', $itGlueTypes['data']);
        }
    }

    public function doCreateType( Request $request ){
        $this->validate( $request, [
            'itGlueConfTypeName' => 'unique:type_maps,itGlueConfTypeName',
            'product_id' => 'required'
        ]);
        $this->itemTypeRepo->create( $request->all() );
        $this->syncGlueServices->reSync();
        return redirect()->back()->with('status', 'Created!');
    }

    public function doEditType( Request $request ){
        $this->itemTypeRepo->update($request->input('type_id'), $request->except('type_id'));
        $this->syncGlueServices->reSync();
        return redirect()->back()->with('status', 'Updated!');
    }

    public function typeDelete( $id ){
        $this->itemTypeRepo->destroy( $id );
        $this->syncGlueServices->reSync();
        return redirect()->back()->with('status', 'Deleted!');
    }

    public function reSyncConfigs(){
        $this->syncGlueServices->reSync();
        return redirect()->back()->with('status', 'Synced!');
    }

    public function doSyncItem(){
        $result = $this->customerServices->updateOrCreateCustomerData();
        $organization_lastsyn = $this->syncyInforRepo->customFind('type' ,'synced');
        $config_lastsyn = $this->syncyInforRepo->customFind('type' ,'configUpdate');
        return view('pages.syncItemsd')->with('result',$result)->with('org_last_update',$organization_lastsyn )->with('conf_last_update', $config_lastsyn);
    }

    public function showBillingReport(){
        $result = $this->customerServices->getCustomerSyncedData();
        return view('pages.billingReport')->with('lastSyncedItems', $result['objectArray'] )->with('pagination',  $result['pagination'] );
    }

    public function getLineItemById( $customer_id ){
        $lineItems = $this->customerServices->getCustomerWithLineItems( $customer_id );
        $result = $this->checkSpecialproduct( $lineItems );

        if( $result ){
            return $result;
        }

        else{
            return false;
        }
    }

    private function checkSpecialproduct( $lineItems ){
        $specil_products =[];
        $rental_products = [];
        $supscription_products = [];
        $service_products = [];
        $rent_total =0;
        $subs_total =0;
        $service_total =0;
        $special_total =0;
        if($lineItems){
            foreach ($lineItems->lineItems as $key => $lineItem){
                switch ( $lineItem->product['product_type'] ){
                    case ConstValues::SPECIAL_PRODUCT:
                        $lineItem['total'] = $lineItem->qty * $lineItem->product['price'];
                        $special_total = $special_total + $lineItem['total'];
                        $specil_products[]= $lineItem;
                        break;
                    case ConstValues::RENTAL_PAYMENT_TYPE:
                        $lineItems->lineItems[$key]->product['tt'] = $lineItem->qty * $lineItem->product['price'];
                        $lineItem['total'] = $lineItem->qty * $lineItem->product['price'];
                        $rent_total = $rent_total + $lineItem['total'];
                        $rental_products[]= $lineItem;
                        break;
                    case ConstValues::SERVICE_PAYMENT_TYPE:
                        $lineItem['total'] = $lineItem->qty * $lineItem->product['price'];
                        $service_total = $service_total + $lineItem['total'];
                        $service_products[]= $lineItem;
                        break;
                    case ConstValues::SUBSCRIPTION_PAYMENT_TYPE:
                        $lineItem['total'] = $lineItem->qty * $lineItem->product['price'];
                        $subs_total = $subs_total + $lineItem['total'];
                        $supscription_products[]=$lineItem;
                        break;
                }

            }
            $gTotal = [
                'rental_total' => $rent_total,
                'subs_total' => $subs_total,
                'service_total' => $service_total,
                'special_total' => $special_total,
            ];



            $lineItems =[
                ConstValues::SPECIAL_PRODUCT => $specil_products,
                ConstValues::RENTAL_PAYMENT_TYPE => $rental_products,
                ConstValues::SERVICE_PAYMENT_TYPE => $service_products,
                ConstValues::SUBSCRIPTION_PAYMENT_TYPE => $supscription_products,
            ];
            $data = [
                'lineItem' => $lineItems,
                'gtotal' => $gTotal
            ];
            return $data;
        }
    }

    private function otherProductOrder( $otherproducts ){
        $otherArray=[];
        foreach ($otherproducts as $product ){
            $product['total'] = $product->qty *$product->product['price'];
            $product_name = $product->product['name'];
            if(array_key_exists( $product_name ,$otherArray)){
                $otherArray[$product_name][]=$product;
                $product->product['total'] = $product->product['total'] + $product['total'];
            }
            else{
                $otherArray[$product_name][]=$product;
                $product->product['total'] = $product['total'];
            }
        }

        return $otherArray;
    }

    public function resetLastUpdate(){

        $types = [
            'lastUpdate',
            'configUpdate'
        ];
        $data = [
            'time'=>'000-00-00 00:00:00'
        ];
        if( $types ){
            foreach ( $types as $type ){
                $this->syncyInforRepo->updateByCustomField('type', $type, $data );
            }
        }
        return redirect()->back()->with('status', 'Reset done!');
    }


    public function acceptDataIntoBillBot( Request $request ){
        $inputs = $request->all();
        $product_ids = $request->input('product_id');
        $qtys = $request->input('qty');
        $details = $request->input('detail');
        $configIds = $request->input('configIds');

        $line_data=[];
        $allLineItems = $this->lineItemRespo->getAllLineItemByCustomerId( $request->input('customer_id') );

        if( $product_ids ){
            foreach ( $allLineItems as $allLineItem){
             if(!in_array( $allLineItem->configIds, $configIds)){
                 $this->lineItemRespo->destroy($allLineItem->id);
             }
            }

            foreach ( $product_ids as $key => $product_id ) {
                $line_data = [
                    'customer_id' => $request->input('customer_id'),
                    'product_id' => $product_id,
                    'qty' => $qtys[$key],
                    'detail' => $details[$key],
                    'configIds' => $configIds[$key],
                ];

                $existingLineItem = $this->lineItemRespo->findLineItemSpecial( $request->input('customer_id') ,$product_id );
                if($existingLineItem){
                    $this->lineItemRespo->updateByCustomerIdAndProductId( $request->input('customer_id'), $product_id, $line_data );
                }
                else{
                    $this->lineItemRespo->create( $line_data );
                }
            }
        }
        else {
            if( $allLineItems ){
                foreach ( $allLineItems as $allLineItem ){
                    // no product ids but have line item mean the configs are deleted ot moved to other status
                    // we have to delete the line items
                    if( $product_ids == null ) {
                        if($allLineItem->product['product_type'] == ConstValues::SPECIAL_PAYMENT_TYPE){
                            $this->lineItemRespo->destroy($allLineItem->id);
                        }
                    }
                    // if product id exist then we have some configs still active
                    // but we need to check each line item with product are exist in the product id array ( which is new synced data from the left column )
                    else{
                        foreach ($product_ids as $product_id){
                            if( $product_id != $allLineItem->product_id && $allLineItem->product['product_type'] == ConstValues::SPECIAL_PAYMENT_TYPE ){
                                $this->lineItemRespo->destroy( $allLineItem->id );
                            }
                        }
                    }
                }
            }
        }

        return redirect()->back()->with('status', 'Accepted data Into BillBot');
    }



    // the below functions are injected to pages so don't delete them without knowing what it does


    public function calculateTotal( $totalArray ){
        $service_total = 0;
        $sub_total = 0;
        $rental_total =0;
        $special_total=0;
        if(isset($totalArray[ConstValues::SPECIAL_PAYMENT_TYPE])){
            foreach ($totalArray[ConstValues::SPECIAL_PAYMENT_TYPE] as $service){
                $service_total = $service_total + $service;
            }
        }
        if(isset($totalArray[ConstValues::SERVICE_PAYMENT_TYPE])){
            foreach ($totalArray[ConstValues::SERVICE_PAYMENT_TYPE] as $service){
                $service_total = $service_total + $service;
            }
        }
        if(isset($totalArray[ConstValues::RENTAL_PAYMENT_TYPE])) {
            foreach ($totalArray[ConstValues::RENTAL_PAYMENT_TYPE] as $rental) {
                $rental_total = $rental_total + $rental;
            }
        }
        if(isset($totalArray[ConstValues::SUBSCRIPTION_PAYMENT_TYPE])) {
            foreach ($totalArray[ConstValues::SUBSCRIPTION_PAYMENT_TYPE] as $subscription) {
                $sub_total = $sub_total + $subscription;
            }
        }
        $service_total = $service_total + $special_total;
        $data = [
            ConstValues::SERVICE_PAYMENT_TYPE=>number_format($service_total, 2),
            ConstValues::RENTAL_PAYMENT_TYPE => number_format($rental_total,2),
            ConstValues::SUBSCRIPTION_PAYMENT_TYPE=> number_format($sub_total,2)
        ];

        return $data ;
    }

    public function calculateSum( $sumArray ){
        $sum =0;
        $tax = $this->settingsRepo->customFind('field_name' , ConstValues::TAX_BILL_SETTINGS);
        $tax = round((float)$tax->field_value/100, 2);

        foreach ($sumArray as $amount){
            $sum = $sum + $amount;
        }
        $subtotal= $sum;

        $taxAmount = $sum * $tax;
        $invoiceTotal = $sum + $taxAmount;

        $data=[
           'tax' => number_format($taxAmount,2),
           'subtotal' => number_format($subtotal,2),
           'invoiceTotal' => number_format($invoiceTotal ,2)
        ];
        return $data;
    }

    public function deleteProduct( $id ){
        if( $id ){
            $lineitem = $this->lineItemRespo->customFind( 'product_id' , $id );
            if($lineitem){
                return redirect()->back()->with('warning', 'This product can not be deleted, System found line items with this product ');
            }
            else{
                $this->productRepo->destroy( $id );
                return redirect()->back()->with('status', 'Deleted!');
            }
        }
    }

    public function clientWideTotal($customers)
    {
        $service_total = 0;
        $subs_total = 0;
        $rental_total = 0;
        $tax = $this->settingsRepo->customFind('field_name', ConstValues::TAX_BILL_SETTINGS);
        $tax = round((float)$tax->field_value / 100, 2);
        // for each customers
        foreach ($customers as $customer) {
            if ($customer) {
                foreach ($customer as $key => $lineItems) {
                    foreach ( $lineItems as $lineItem){
                        switch ($key) {
                            case ConstValues::RENTAL_PAYMENT_TYPE:
                                $rental_total = $rental_total + $lineItem->total;
                                break;
                            case ConstValues::SUBSCRIPTION_PAYMENT_TYPE:
                                $subs_total = $subs_total + $lineItem->total;
                                break;
                            case ConstValues::SERVICE_PAYMENT_TYPE:
                            case ConstValues::SPECIAL_PAYMENT_TYPE:
                                $service_total = $service_total + $lineItem->total;
                                break;
                        }
                    }
                }
            }
        }
        $subTotal = $subs_total + $rental_total + $service_total;
        $cjtax = $subTotal * $tax;
        $invoiceTotal = $subTotal + $cjtax;
        //// End customers loop
        $data = [
            ConstValues::SUBSCRIPTION_PAYMENT_TYPE => number_format($subs_total,2),
            ConstValues::RENTAL_PAYMENT_TYPE => number_format($rental_total,2),
            ConstValues::SERVICE_PAYMENT_TYPE => number_format($service_total,2),
            ConstValues::CLIENT_WIDE_SUBTOTAL => number_format($subTotal,2),
            ConstValues::CLIENT_WIDE_CBJ_TAX => number_format($cjtax,2),
            ConstValues::CLIENT_WIDE_INVOICE => number_format($invoiceTotal,2)
        ];
        return $data;
    }

    public function deleteLineItem( Request $request ){
        $customer_id = $request->input('customer_id');
        $results = $this->configRepo->getAllConfigForCustomerId( $customer_id );
        foreach ($results as $key => $result){
            if ($result->itGlueStatus == ConstValues::CONFIG_DECOMMISSIONED){
                unset($results[$key]);
            }
        }
        if(count($results)<= 0){
            $lineItems = $this->lineItemRespo->getAllLineItemByCustomerId( $customer_id );
            foreach ( $lineItems as $lineItem ){
                if( $lineItem )$this->lineItemRespo->destroy( $lineItem->id );
            }
            return redirect()->back()->with('status', 'Accepted data Into BillBot');
        }
    }

    public function getPendingSummary( $modified_configs , $lineitems){

        $products = $this->productRepo->listOfProductTypeBySingleType( ConstValues::SPECIAL_PAYMENT_TYPE );
        $lineArray = [];
        foreach ($lineitems['special'] as $item){
            foreach ($item['configs'] as $config){
                if($config['icon'] == 'fa-remove redtick'){
                    $lineArray[$item->id]=$item;
                }
            }
        }
        $configArray =[];
        foreach ( $modified_configs as $modified_config){
            foreach ( $modified_config['item'] as $item){
                if($item->product['product_type'] == ConstValues::SPECIAL_PAYMENT_TYPE  && $item->icon == 'fa-plus greentick'){
                    $configArray[]= $item;
                }
            }
        }
        $merged_items = array_merge($lineArray ,$configArray);
        $new_array=[];
        foreach ( $products as $key => $product){
            foreach ( $merged_items as $merged_item){
                if($merged_item->product['id'] == $key){
                    $new_array[$product]['item'][]= $merged_item;
                    if(isset($merged_item->configs)){
                        $new_array[$product]['total'] = count( $merged_item->configs ) * $merged_item->product['price'];
                    }
                    else{
                        $new_array[$product]['total'] = count( $new_array[$product]['item'] ) * $merged_item->product['price'];
                    }

                }
            }

        }
        return $new_array;

    }
}
