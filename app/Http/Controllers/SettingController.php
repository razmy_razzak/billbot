<?php

namespace App\Http\Controllers;

use App\Repository\SettingRepository;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    private $settingsRepo;

    public function __construct(
        SettingRepository $settingRepository
    )
    {
        $this->settingsRepo = $settingRepository;
    }

    public function index(){
        $settings = $this->settingsRepo->paginate();
        return view('settings.settings_index')->with( 'settings', $settings );
    }

    public function showSettingsForm( $id =null ){
        if( $id ){
            $setting = $this->settingsRepo->find( $id );
            return view('settings.editSetting')->with('setting', $setting);
        }
        else{
            return view('settings.createSetting');
        }
    }

    public function createSetting( Request $request ){
        $this->validate($request,[
            'field_value' => 'required'
        ]);

        $this->settingsRepo->create( $request->all());
        return redirect()->back()->with('status', 'Created!');
    }

    public function editSetting( Request $request ){
        $this->validate($request,[
            'field_value' => 'required'
        ]);

        $settings_id = $request->input('settings_id');
        $this->settingsRepo->update( $settings_id, $request->except('settings_id'));
        return redirect()->back()->with('status', 'Updated!');
    }
}
