<?php

namespace App\Http\Controllers;

use App\Objects\UserClass;
use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterContoller extends Controller
{
    private $userRepo;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepo = $userRepository;
    }

    public function showRegister(){
        return view('pages.register_form');
    }

    public function showAdminEdit( $user_id  ){
        $admin = $this->userRepo->find( $user_id );
        return view('pages.adminEdit')->with( 'user', $admin );
    }

    public function updateAdmin( Request $request ){

        $this->validate($request,[
            'name' => 'required',
            'email' => 'email|required|unique:users,email',
        ]);

        if( !empty( $request->input('password'))){
            $this->validate($request,[
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required|min:8'
            ]);
        }
        $user = new UserClass( $request );
        try{
            $this->userRepo->update( Auth::user()->id, (array)$user );
            return redirect()->back()->with('status', 'Updated!');
        }
        catch (\Exception $e ){
            abort(404, $e->getMessage());
        }
    }
}
