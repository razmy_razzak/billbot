<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'configs';

    protected $guarded = [];

    protected $primaryKey = 'itGlueId';

    public function product(){
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }
}
