<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 28/07/2018
 * Time: 05:55
 */

namespace App\RepoInterfaces;


interface ConfigInterface extends RepositoryInterface
{
    public function getAllConfigForCustomerId( $customer_id );
    public function getConfigsIdsForCustomerId( $customer_id );

}