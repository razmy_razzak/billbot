<?php

namespace App\RepoInterfaces;

interface RepositoryInterface
{
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function updateByCustomField($column, $value, array $attributes );
    public function destroy($id);

    public function all($columns = array('*'));
    public function paginate($perPage = 15, $columns = array('*'));
    public function find($id, $columns = array('*'));

    public function customFind($column_name, $value);

    public function countRow();

}