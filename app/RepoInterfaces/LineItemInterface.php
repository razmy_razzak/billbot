<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 08/08/2018
 * Time: 10:15
 */

namespace App\RepoInterfaces;


interface LineItemInterface extends RepositoryInterface
{
    public function findItemLineWithCustomerById( $id );
    public function getAllItemLineWithRelation( $pagination =10);
    public function getLineItemByCustomerId( $customer_id );
    public function findLineItemSpecial( $customer_id, $product_id );
    public function updateByCustomerIdAndProductId($customer_id, $product_id, array $attributes);
    public function getConfigIdsForCustomerId( $customer_id );
    public function getAllLineItemByCustomerId( $customer_id );
}