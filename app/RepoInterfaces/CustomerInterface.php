<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 18:22
 */

namespace App\RepoInterfaces;


interface CustomerInterface extends RepositoryInterface
{
    public function getAllCustomersWithConfigs ( $pagination = 10);
    public function getAllCustomersIds();
    public function getCustomersWithLineItems( $customer_id );
}