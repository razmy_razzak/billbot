<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 30/07/2018
 * Time: 16:50
 */

namespace App\RepoInterfaces;


interface ItemTypeInterface extends RepositoryInterface
{
    public function getTypesByAlpha( $perPage = 15 );

}