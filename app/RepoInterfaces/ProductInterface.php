<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 30/07/2018
 * Time: 15:01
 */

namespace App\RepoInterfaces;


interface ProductInterface extends RepositoryInterface
{
    public function listOfProducts();
    public function getProductsByTypes( $column, $valueOne, $valueTwo );
    public function getProductsForLineItems();
    public function listOfProductTypeBySingleType( $type );
}