<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 25/07/2018
 * Time: 18:09
 */

namespace App\RepoInterfaces;


interface SyncInfoInterface extends RepositoryInterface
{
    public function triggerUpdate( $value );

}