<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItems extends Model
{
    protected $table = 'line_items';

    protected $guarded = [];


    public function product(){
        return $this->belongsTo('App\Products', 'product_id', 'id');
    }

    public function customer(){
        return $this->belongsTo('App\Customers', 'customer_id', 'itGlueId');
    }
}
