<?php

namespace App\Console\Commands;

use App\Objects\CustomerClass;
use App\Repository\CustomerRepository;
use App\Services\CustomerService;
use App\Services\ItGlueServices;
use Illuminate\Console\Command;

class UpdateCustomerITGlue extends Command
{
    private $itGlueServices;
    private $customerRepo;
    private $customerServices;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ITGlue:updateCustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get latest update for customers form ITGlue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ItGlueServices $itGlueServices,
        CustomerRepository $customerRepository,
        CustomerService $customerService
    )
    {
        parent::__construct();
        $this->itGlueServices = $itGlueServices;
        $this->customerRepo = $customerRepository;
        $this->customerServices = $customerService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = $this->customerServices->updateOrCreateCustomerData();
        var_dump($result);

    }
}
