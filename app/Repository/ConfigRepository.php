<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 28/07/2018
 * Time: 05:56
 */

namespace App\Repository;


use App\Objects\ConstValues;
use App\RepoInterfaces\ConfigInterface;

class ConfigRepository extends AbstractRepository implements ConfigInterface
{
    function model()
    {
        return 'App\Config';
    }

    public function getAllConfigForCustomerId($customer_id)
    {
        return $this->model->where('itGlueCustomerId',$customer_id )->with('product')->get();
    }

    public function getConfigsIdsForCustomerId($customer_id)
    {
        return $this->model->where('itGlueCustomerId',$customer_id )->where('itGlueStatus', ConstValues::CONFIG_ACTIVE)->pluck("itGlueId")->toArray();
        // TODO: Implement getConfigsIdsForCustomerId() method.
    }

}