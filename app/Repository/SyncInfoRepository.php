<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 25/07/2018
 * Time: 18:10
 */

namespace App\Repository;


use App\RepoInterfaces\SyncInfoInterface;

class SyncInfoRepository extends AbstractRepository implements SyncInfoInterface
{

    function model()
    {
        return 'App\SyncyInfo';
    }

    public function triggerUpdate( $id )
    {
        return true;
    }

}