<?php
namespace App\Repository;


use App\RepoInterfaces\RepositoryInterface;
use Illuminate\Container\Container;

abstract class AbstractRepository implements RepositoryInterface
{
    private $app;
    protected $model;

    public function  __construct( Container $app )
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model();



    public function makeModel() {
        $model = $this->app->make($this->model());
        return $this->model = $model;
    }

    public function find($id, $columns = array('*'))
    {
        return $this->model->find( $id );
    }

    public function create(array $attributes)
    {
        $data = $this->model->create( $attributes );
        if( $data ) return $data ;
    }


    public function all( $columns = array('*') )
    {
        return $this->model->all();
    }

    public function destroy($id)
    {
        $data = $this->find( $id );
        $data->delete();
        return true;
    }

    public function paginate( $perPage = 15, $columns = array('*') )
    {
        $data = $this->model->paginate( $perPage );
        if ($data ) return $data;
    }

    public function update($id, array $attributes)
    {
        $data = $this->find($id);
        $data->update( $attributes );
        return true;
    }

    public function customFind($column_name, $value)
    {
        return $this->model->where( $column_name, $value)->first();
    }

    public function countRow()
    {
        return $this->model->count();
    }

    public function updateByCustomField($column, $value, array $attributes)
    {
        $data = $this->customFind( $column, $value );
        $data->update( $attributes );
        return true;
    }


}