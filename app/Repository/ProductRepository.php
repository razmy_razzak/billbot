<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 30/07/2018
 * Time: 15:02
 */

namespace App\Repository;


use App\Objects\ConstValues;
use App\RepoInterfaces\ProductInterface;

class ProductRepository extends AbstractRepository implements ProductInterface
{

    function model()
    {
        return 'App\Products';
    }

    public function listOfProducts()
    {
        return $this->model->where('product_type', ConstValues::SPECIAL_PAYMENT_TYPE)->orWhere('product_type', ConstValues::NORMAL_PRODUCT)->pluck("name","id");
    }

    public function getProductsByTypes($column, $valueOne, $valueTwo)
    {
        return $this->model->where( $column , $valueOne)->orWhere($column, $valueTwo)->get();
    }

    public function getProductsForLineItems()
    {
        return $this->model->where('product_type', ConstValues::SUBSCRIPTION_PAYMENT_TYPE)->orWhere('product_type', ConstValues::RENTAL_PAYMENT_TYPE)->orWhere('product_type', ConstValues::SERVICE_PAYMENT_TYPE)->get();
    }

    public function listOfProductTypeBySingleType($type)
    {
        return $this->model->where( 'product_type',$type )->pluck("name","id");
    }


}