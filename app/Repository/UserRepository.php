<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 10:47
 */

namespace App\Repository;


use App\RepoInterfaces\UserInterface;

class UserRepository extends AbstractRepository implements UserInterface
{
    function model()
    {
        return 'App\User';
    }
}