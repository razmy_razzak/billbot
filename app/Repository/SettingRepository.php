<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 10/08/2018
 * Time: 10:54
 */

namespace App\Repository;


use App\RepoInterfaces\SettingInterface;

class SettingRepository extends AbstractRepository implements SettingInterface
{
    function model()
    {
        return 'App\Settings';
    }

}