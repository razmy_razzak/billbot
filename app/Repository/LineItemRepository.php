<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 08/08/2018
 * Time: 10:16
 */

namespace App\Repository;


use App\RepoInterfaces\ItemTypeInterface;
use App\RepoInterfaces\LineItemInterface;

class LineItemRepository extends AbstractRepository implements LineItemInterface
{
    function model()
    {
        return 'App\LineItems';
    }

    public function findItemLineWithCustomerById( $id )
    {
        return $this->model->with('customer')->with('product')->find( $id );
    }

    public function getAllItemLineWithRelation( $pagination = 10 ){
        return $this->model->with('customer')->with('product')->paginate( $pagination );
    }

    public function getLineItemByCustomerId($customer_id)
    {
        return $this->model->with('product')->where('customer_id', $customer_id)->first();
    }

    public function findLineItemSpecial( $customer_id, $product_id )
    {
        return $this->model->where('customer_id' , $customer_id)->where('product_id', $product_id)->first();
    }

    public function updateByCustomerIdAndProductId( $customer_id, $product_id , array $attributes )
    {
        $lineItem = $this->findLineItemSpecial( $customer_id, $product_id );
        $lineItem->update( $attributes );
        return true;

    }

    public function getConfigIdsForCustomerId($customer_id)
    {
        $configIds = [];
        $lineItems = $this->model->where('customer_id', $customer_id )->get();
        foreach ( $lineItems as $lineItem ){
            $ids = explode(',' ,$lineItem->configIds);
            $configIds = array_merge($configIds, $ids);
        }
        return $configIds;

    }

    public function getAllLineItemByCustomerId($customer_id)
    {
        return $this->model->with('product')->where('customer_id', $customer_id)->get();
    }

}