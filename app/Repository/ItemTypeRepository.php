<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 30/07/2018
 * Time: 16:51
 */

namespace App\Repository;


use App\RepoInterfaces\ItemTypeInterface;

class ItemTypeRepository extends AbstractRepository implements ItemTypeInterface
{

    function model()
    {
        return 'App\TypeMap';
    }

    public function getTypesByAlpha($perPage = 100)
    {
        return $this->model->orderBy('itGlueConfTypeName', 'ASC')->paginate( $perPage );
    }


}