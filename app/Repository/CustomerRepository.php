<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 18:23
 */

namespace App\Repository;


use App\Objects\ConstValues;
use App\RepoInterfaces\CustomerInterface;

class CustomerRepository extends AbstractRepository implements CustomerInterface
{

    function model()
    {
        return 'App\Customers';
    }

    public function getAllCustomersWithConfigs($pagination = 2)
    {
        return $this->model->where('itGlueStatus', ConstValues::CUSTOMER_ACTIVE )->with('configs.product')->paginate( $pagination );
    }

    public function getAllCustomersIds()
    {
        return $this->model->pluck("itGlueName","itGlueId")->all();
    }

    public function getCustomersWithLineItems($customer_id)
    {
        return $this->model->with('lineItems.product')->where('itGlueId', $customer_id )->first();
    }


}