<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';

    protected $guarded = [];

    protected $primaryKey = 'itGlueId';

    public function configs(){
        return $this->hasMany('App\Config' ,'itGlueCustomerId' ,'itGlueId');
    }

    public function lineItems(){
        return $this->hasMany('App\LineItems' ,'customer_id' ,'itGlueId');
    }
}
