<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyncyInfo extends Model
{
    protected $table = 'syncy_infos';

    protected $guarded = [];
}
