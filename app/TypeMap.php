<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeMap extends Model
{
    protected $table = 'type_maps';

    protected $guarded = [];
}
