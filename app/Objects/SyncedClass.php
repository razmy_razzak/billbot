<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 01/08/2018
 * Time: 11:41
 */

namespace App\Objects;


use App\Config;
use App\Repository\LineItemRepository;

class SyncedClass
{
    //customer details
    public $customerName;
    public $customerId;
    public $tax;

    //configuration details
    public $configuration =[];
    public $devices = [];
    public $computers =[];
    public $others =[];

    public $active_products=[];
    public $pre_deploy_products =[];
    public $decommissioned =[];
    public $unClssified_prodcts =[];

    public $lineItemRepo;

    public $icon;


    public function __construct( $data , $products, LineItemRepository $lineItemRepository )
    {
        $dev_price = 0;
        $com_price =0;
        $this->customerName = $data->itGlueName;
        $this->customerId = $data->itGlueId;
        $this->tax = $data->tax;
        $this->lineItemRepo = $lineItemRepository;
        $line_item_confgiIds = $this->lineItemRepo->getConfigIdsForCustomerId( $this->customerId );
        foreach ( $products as $key => $product ) {
            if ($product->product_type == ConstValues::SPECIAL_PRODUCT || $product->product_type == ConstValues::NORMAL_PRODUCT) {
            /**
             * @var  $keytwo
             * @var   Config $confg
             */
            foreach ($data->configs as $keytwo => $confg) {
                if ($confg->product) {
                    switch ($confg->itGlueStatus) {
                        case 'Active':
                            if ($product->id == $confg->product['id']) {
                                $this->active_products[$product->name]['item'][] = $confg;
                                if( in_array( $confg->itGlueId, $line_item_confgiIds )){
                                    $confg->setAttribute('icon', 'fa-check bluetick');
                                }
                                else{
                                    $confg->setAttribute('icon', 'fa-plus greentick');
                                }

                                unset($data->configs[$keytwo]);
                            }
                            break;
                        case 'Pre-Deploy' :
                            $this->pre_deploy_products[] = $confg;
                            unset($data->configs[$keytwo]);
                            break;
                        case 'Decommissioned' :
                            $this->decommissioned[] = $confg;
                            unset($data->configs[$keytwo]);
                            break;
                        default :
                            '';
                    }
                }
                else {
                    if($confg->itGlueStatus != 'Decommissioned' ){
                        $this->unClssified_prodcts[] = $confg;
                    }
                    unset($data->configs[$keytwo]);
                }
                }
            }
        }

        foreach ( $this->active_products as $key => $product ){
            $count =0;
            $price =0;
            $count =  count( $product['item'] );
            if( $count >= 0){
                $price = $product['item'][0]->product['price'];
            }
            $this->active_products[$key]['count'] = $count;
            $this->active_products[$key]['total'] = $price * $count;
        }

    }


    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return array
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * @param array $devices
     */
    public function setDevices($devices)
    {
        $this->devices = $devices;
    }

    /**
     * @return array
     */
    public function getComputers()
    {
        return $this->computers;
    }

    /**
     * @param array $computers
     */
    public function setComputers($computers)
    {
        $this->computers = $computers;
    }

    /**
     * @return array
     */
    public function getOthers()
    {
        return $this->others;
    }

    /**
     * @param array $others
     */
    public function setOthers($others)
    {
        $this->others = $others;
    }

    /**
     * @return array
     */
    public function getActiveProducts()
    {
        return $this->active_products;
    }

    /**
     * @param array $active_products
     */
    public function setActiveProducts($active_products)
    {
        $this->active_products = $active_products;
    }

    /**
     * @return array
     */
    public function getPreDeployProducts()
    {
        return $this->pre_deploy_products;
    }

    /**
     * @param array $pre_deploy_products
     */
    public function setPreDeployProducts($pre_deploy_products)
    {
        $this->pre_deploy_products = $pre_deploy_products;
    }

    /**
     * @return array
     */
    public function getUnClssifiedProdcts()
    {
        return $this->unClssified_prodcts;
    }

    /**
     * @param array $unClssified_prodcts
     */
    public function setUnClssifiedProdcts($unClssified_prodcts)
    {
        $this->unClssified_prodcts = $unClssified_prodcts;
    }

    /**
     * @return array
     */
    public function getDecommissioned()
    {
        return $this->decommissioned;
    }

    /**
     * @param array $decommissioned
     */
    public function setDecommissioned($decommissioned)
    {
        $this->decommissioned = $decommissioned;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }




}