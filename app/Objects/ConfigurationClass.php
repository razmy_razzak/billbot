<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 26/07/2018
 * Time: 16:51
 */

namespace App\Objects;


use App\Repository\ItemTypeRepository;

class ConfigurationClass
{
    private $itGlueId;
    private $itGlueCustomerId;
    private $itGlueStatus;
    private $itGlueName;
    private $itGlueType;
    private $itGlueTypeId;
    private $product_id;
    private $update_at;
    private $itemTypeRepo;

    public function __construct(
        $data,
        ItemTypeRepository $itemTypeRepository
    )
    {
        $this->itGlueId = $data->id;
        $this->itGlueCustomerId = $data->attributes->{'organization-id'}? $data->attributes->{'organization-id'} : '';
        $this->itGlueStatus = $data->attributes->{'configuration-status-name'}? $data->attributes->{'configuration-status-name'}: '';
        $this->itGlueName = $data->attributes->name;
        $this->itGlueType = $data->attributes->{'configuration-type-name'}?$data->attributes->{'configuration-type-name'}:'';
        $this->itGlueTypeId = $data->attributes->{'configuration-type-id'}?$data->attributes->{'configuration-type-id'}:'';
        $this->update_at = new \DateTime($data->attributes->{'updated-at'});
        $this->itemTypeRepo = $itemTypeRepository;

        $itemType = $this->itemTypeRepo->customFind('itGlueConfTypeId' , $this->itGlueTypeId );
        if( $itemType ){
            $this->product_id =  $itemType->product_id;
        }
        else {
            $this->product_id = 0;
        }

    }

    /**
     * @return mixed
     */
    public function getItGlueId()
    {
        return $this->itGlueId;
    }

    /**
     * @param mixed $itGlueId
     */
    public function setItGlueId($itGlueId)
    {
        $this->itGlueId = $itGlueId;
    }

    /**
     * @return mixed
     */
    public function getItGlueStatus()
    {
        return $this->itGlueStatus;
    }

    /**
     * @param mixed $itGlueStatus
     */
    public function setItGlueStatus($itGlueStatus)
    {
        $this->itGlueStatus = $itGlueStatus;
    }

    /**
     * @return mixed
     */
    public function getItGlueName()
    {
        return $this->itGlueName;
    }

    /**
     * @param mixed $itGlueName
     */
    public function setItGlueName($itGlueName)
    {
        $this->itGlueName = $itGlueName;
    }

    /**
     * @return mixed
     */
    public function getItGlueType()
    {
        return $this->itGlueType;
    }

    /**
     * @param mixed $itGlueType
     */
    public function setItGlueType($itGlueType)
    {
        $this->itGlueType = $itGlueType;
    }

    /**
     * @return mixed
     */
    public function getItGlueCustomerId()
    {
        return $this->itGlueCustomerId;
    }

    /**
     * @param mixed $itGlueCustomerId
     */
    public function setItGlueCustomerId($itGlueCustomerId)
    {
        $this->itGlueCustomerId = $itGlueCustomerId;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
    }

    /**
     * @return mixed
     */
    public function getItGlueTypeId()
    {
        return $this->itGlueTypeId;
    }

    /**
     * @param mixed $itGlueTypeId
     */
    public function setItGlueTypeId($itGlueTypeId)
    {
        $this->itGlueTypeId = $itGlueTypeId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }



}