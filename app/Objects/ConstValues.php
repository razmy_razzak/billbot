<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 07/08/2018
 * Time: 12:24
 */

namespace App\Objects;


class ConstValues
{
    //system product types
    // at this stage we don't need a CRUD for this types
    const SPECIAL_PRODUCT = 'special';
    const NORMAL_PRODUCT = 'normal';
    const BILLING_PRODUCT = 'billing';


    const SERVICE_PAYMENT_TYPE = 'service';
    const RENTAL_PAYMENT_TYPE = 'rental';
    const SUBSCRIPTION_PAYMENT_TYPE = 'subscription';
    const SPECIAL_PAYMENT_TYPE = 'special';


    const TAX_BILL_SETTINGS ='tax_bill_amount_settings';
    const BILL_PAGE_PAGINATION = 'bill_page_pagination';

    //Client wide total
    const CLIENT_WIDE_SUBTOTAL ='Subtotal';
    const CLIENT_WIDE_CBJ_TAX = 'Cbjtax';
    const CLIENT_WIDE_INVOICE = 'invoice';


    //configs status
    const CONFIG_ACTIVE ='Active';
    const CONFIG_DECOMMISSIONED  ='Decommissioned';
    const CONFIG_PRE_DEPLOYED ='Pre-Deploy';


    //customer activa or inactive
    const CUSTOMER_ACTIVE = 'Active';
    const CUSTOMER_INACTIVE ='Inactive';
}