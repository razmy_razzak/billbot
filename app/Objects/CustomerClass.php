<?php
/**
 * Created by PhpStorm.
 * User: mafa
 * Date: 23/07/2018
 * Time: 18:26
 */

namespace App\Objects;


class CustomerClass
{
    public $itGlueId;
    public $itGlueName;
    public $itGlueUpdated;
    public $itGlueCreated;
    public $itGlueType;
    public $itGlueStatus;
    public $tax;

    public function __construct( $data )
    {
        $this->itGlueId = $data->id;
        $this->itGlueName = $data->attributes->name;
        $this->itGlueUpdated = new \DateTime($data->attributes->{'updated-at'});
        $this->itGlueCreated = new \DateTime($data->attributes->{'created-at'});
        $this->itGlueType = $data->attributes->{'organization-type-name'};
        $this->itGlueStatus = $data->attributes->{'organization-status-name'};
        $this->tax = true;
    }

    /**
     * @return mixed
     */
    public function getItGlueId()
    {
        return $this->itGlueId;
    }

    /**
     * @param mixed $itGlueId
     */
    public function setItGlueId($itGlueId)
    {
        $this->itGlueId = $itGlueId;
    }

    /**
     * @return mixed
     */
    public function getItGlueName()
    {
        return $this->itGlueName;
    }

    /**
     * @param mixed $itGlueName
     */
    public function setItGlueName($itGlueName)
    {
        $this->itGlueName = $itGlueName;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return mixed
     */
    public function getItGlueUpdated()
    {
        return $this->itGlueUpdated;
    }

    /**
     * @param mixed $itGlueUpdated
     */
    public function setItGlueUpdated($itGlueUpdated)
    {
        $this->itGlueUpdated = $itGlueUpdated;
    }

    /**
     * @return mixed
     */
    public function getItGlueCreated()
    {
        return $this->itGlueCreated;
    }

    /**
     * @param mixed $itGlueCreated
     */
    public function setItGlueCreated($itGlueCreated)
    {
        $this->itGlueCreated = $itGlueCreated;
    }

    /**
     * @return mixed
     */
    public function getItGlueType()
    {
        return $this->itGlueType;
    }

    /**
     * @param mixed $itGlueType
     */
    public function setItGlueType($itGlueType)
    {
        $this->itGlueType = $itGlueType;
    }

    /**
     * @return mixed
     */
    public function getItGlueStatus()
    {
        return $this->itGlueStatus;
    }

    /**
     * @param mixed $itGlueStatus
     */
    public function setItGlueStatus($itGlueStatus)
    {
        $this->itGlueStatus = $itGlueStatus;
    }



}