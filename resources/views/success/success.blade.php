@if (session('status'))
    <div id="success" class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if (session('warning'))
    <div id="success" class="alert alert-warning">
        {{ session('warning') }}
    </div>
@endif