@php( $tottalArray=[])
@foreach( $result['lineItem'][\App\Objects\ConstValues::SPECIAL_PAYMENT_TYPE] as $lineItem  )
    <div class="">
        <strong>{{$lineItem->product['name']}}</strong>
        <strong class="floatRight">{{$lineItem->qty}} | $ {{$lineItem['total']}} </strong>
        <hr class="item_line">
    </div>
    @php($tottalArray[\App\Objects\ConstValues::SPECIAL_PAYMENT_TYPE][]= $lineItem['total'] )
    <ul class="list-unstyled space_botom">
        @foreach( $lineItem->configs as $item)
            <li>{{$item['name']}} <i class="fa {{$item['icon']}} floatRight"></i></li>
        @endforeach
    </ul>
@endforeach
<hr class="item_line">
<hr class="item_line">


@if( count( $result['lineItem'][\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE] ) > 0)
    @include('parts.line_item',['payment_typ'=> \App\Objects\ConstValues::RENTAL_PAYMENT_TYPE ,'title'=>'Rental','total' =>$result['gtotal']['rental_total'] , 'items' => $result['lineItem'][\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE]])
    @php($tottalArray[\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE][]= $result['gtotal']['rental_total'] )
@endif

@if( count( $result['lineItem'][\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE] ) > 0)
    @include('parts.line_item',['payment_typ'=> \App\Objects\ConstValues::SERVICE_PAYMENT_TYPE ,'title'=>'Services','total' =>$result['gtotal']['service_total'] , 'items' => $result['lineItem'][\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE]])
    @php($tottalArray[\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE][]= $result['gtotal']['service_total'] )
@endif

@if( count( $result['lineItem'][\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE] ) > 0)
    @include('parts.line_item',['payment_typ'=> \App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE ,'title'=>'Subscription','total' =>$result['gtotal']['subs_total'] , 'items' => $result['lineItem'][\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE]])
    @php($tottalArray[\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE][]= $result['gtotal']['subs_total'])
@endif


<hr class="item_devider">
<hr class="item_devider">

@inject('calculateTotal', 'App\Http\Controllers\ProductController')
@php( $result = $calculateTotal->calculateTotal($tottalArray))

<div class="timeline-panel">
    <div class="timeline-heading">
        <h4 class="timeline-title">{{$lastSyncedItem->customerName}} ,Billing Summary</h4>
    </div>
    <div class="timeline-body">
        <ul class="list-unstyled space_botom">
            @inject('calculateSum', 'App\Http\Controllers\ProductController')
            @php( $sumArray =[])
            @foreach($result as $key => $item)
                <li>{{$key}} <span class="floatRight">$ {{$item}}</span></li>
                @php( $sumArray[]=$item )
            @endforeach
            @php( $result = $calculateSum->calculateSum($sumArray))
        </ul>
        Subtotal <span class="floatRight">$ {{$result['subtotal']}}</span>
        <hr class="item_line">
        <p>CBJ Sales Tax: <span class="floatRight"> $ {{$result['tax']}}</span></p>
        <strong>Invoice Total <span class="floatRight">$ {{$result['invoiceTotal']}}</span></strong>
    </div>
</div>
