<div class="">
    <strong>{{$config_title}}</strong>
    <span class="floatRight">N/A </span>
    <hr class="item_line">
</div>

<ul class="list-unstyled space_botom">
    @foreach( $config_items as $item)
        <li>{{$item->itGlueId}}  {{$item->itGlueName}} ( {{$item->itGlueType}} ) <i class="fa fa-minus-circle floatRight"></i></li>
    @endforeach
</ul>
