@foreach( $specialProducts as $key => $product )
    @php($array = [])
    @php($configIds=[])
<div class="">
    <strong>{{$key}}</strong>
    <span class="floatRight">{{$product['count']}} | $ {{$product['total']}} </span>
    <hr class="item_line">
</div>

<ul class="list-unstyled space_botom">
        @foreach( $product['item'] as $item)
            <li>{{$item->itGlueId}}  {{$item->itGlueName}} <i class="fa {{$item->icon}} floatRight"></i></li>
            @php( $array[]= $item->itGlueId.' '. $item->itGlueName  )
            @php( $configIds[]= $item->itGlueId )
        @endforeach
    </ul>
        @php( $detail = implode(',' ,$array))
        @php( $ids = implode(',' ,$configIds))
        <input type="hidden" name="product_id[{{$key}}]" value="{{$item->product['id']}}">
        <input type="hidden" name="qty[{{$key}}]" value="{{$product['count']}}">
        <input type="hidden" name="detail[{{$key}}]" value="{{$detail}}">
        <input type="hidden" name="configIds[{{$key}}]" value="{{$ids}}">
        <input type="hidden" name="paymentType[{{$key}}]" value="{{\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE}}">
@endforeach

