<div class="">
    <strong>{{$title}}</strong>
    <strong class="floatRight">$ {{$total}} </strong>
    <hr class="item_line">
</div>
<ul class="list-unstyled space_botom">
    @foreach( $items as $item )
        <li>
            {{$item->product['name']}} <span class="floatRight">{{$item->qty}} | $ {{$item['total']}}</span>
            <p class="note">"{{$item->note}}"</p>
        </li>
    @endforeach
</ul>