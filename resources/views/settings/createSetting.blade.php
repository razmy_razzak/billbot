@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create Settings
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('createSetting') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('field_value') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">{{\App\Objects\ConstValues::TAX_BILL_SETTINGS}}</label>
                            <input name="field_name" type="hidden" value="{{\App\Objects\ConstValues::TAX_BILL_SETTINGS}}">

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="field_value" value="{{ old('field_value') }}" required autofocus>

                                @if ($errors->has('field_value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('field_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection