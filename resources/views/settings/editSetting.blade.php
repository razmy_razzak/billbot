@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Settings
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('editSetting') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="settings_id" value="{{$setting->id}}" >

                        <div class="form-group{{ $errors->has('field_value') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">{{$setting->field_name}}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="field_value" value="{{$setting->field_value}}" required autofocus>

                                @if ($errors->has('field_value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('field_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection