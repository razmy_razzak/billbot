@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Map product to Config type
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('doCreateType') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('itGlueConfTypeName') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select ItGLue Config Type</label>
                            <input id="itGlueConfTypeName" name="itGlueConfTypeName" type="hidden" >
                            <div class="col-md-6">

                                <select class="form-control product" name="itGlueConfTypeId">
                                    @foreach( $itGlueTypes as $itGlueType )
                                        <option data-value="{{ $itGlueType->attributes->{'name'} }}" value="{{$itGlueType->id}}">{{ $itGlueType->attributes->{'name'}  }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('itGlueConfTypeName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('itGlueConfTypeName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Product</label>
                            <input id="product_name" name="product_name" type="hidden" >
                            <div class="col-md-6">
                                <select class="form-control type" name="product_id">
                                    <option  value="" >Select</option>
                                    @foreach( $products as $key =>$product )
                                        <option data-value="{{$product}}" value="{{$key}}">{{$product}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('product_id'))
                                    <span class="help-block">
                                        <strong>Please select product</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection