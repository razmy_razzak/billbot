@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Synced Inforamtion</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-body">
                    <pre>Last Synced time: {{ date('F d, Y H:i:s', strtotime($org_last_update->updated_at)) }}</pre>
                    <div class="col-lg-6">

                        <div>
                            {{--{{var_dump($result['config_data'])}}--}}
                            <ul class="list-unstyled">
                                <li><h3>Organizations</h3></li>
                                <li>Unchanged: {{$result['customer_data']['Unchanged']}} </li>
                                <li>Created: {{$result['customer_data']['New']}} </li>
                                <li>Removed: {{$result['customer_data']['removed_customers']}} </li>
                                <li>Updated: {{$result['customer_data']['Changed']}} </li>
                            </ul>
                            <ul class="list-unstyled">
                                <li><strong>Created items</strong></li>

                                @foreach( $result['customer_data']['created_list']  as $item )
                                    <li>{{ $item }} </li>
                                @endforeach
                            </ul>

                            <ul class="list-unstyled">
                                <li><strong>Updated items</strong></li>

                                @foreach( $result['customer_data']['updated_list']  as $item )
                                    <li>{{ $item }} </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div>
                            {{--{{var_dump($result['config_data'])}}--}}
                            <ul class="list-unstyled">
                                <li><h3>Configurations</h3></li>
                                <li>Unchanged: {{$result['config_data']['Unchanged']}} </li>
                                <li>Created: {{$result['config_data']['New']}} </li>
                                <li>Removed: {{$result['config_data']['Removed']}} </li>
                                <li>Updated: {{$result['config_data']['Changed']}} </li>
                            </ul>

                            <ul class="list-unstyled">
                                <li><strong>Created items</strong></li>

                                @foreach( $result['config_data']['created_list']  as $item )
                                    <li>{{ $item }} </li>
                                @endforeach
                            </ul>

                            <ul class="list-unstyled">
                                <li><strong>Updated items</strong></li>

                                @foreach( $result['config_data']['updated_list']  as $item )
                                    <li>{{ $item }} </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection