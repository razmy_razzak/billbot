@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Line Item
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('updateLineItem') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$lineItem->id}}">

                        <div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Customer</label>
                            <div class="col-md-6">
                                <select class="form-control" name="customer_id">
                                    @foreach($customers as $key => $customer)
                                        <option @if($lineItem->customer['itGlueName'] == $customer )selected @endif  value="{{$key}}">{{$customer}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Product</label>
                            <div class="col-md-6">
                                <select class="form-control" name="product_id">
                                    @foreach($products as $key => $product)
                                        <option @if($lineItem->product_id == $product->id ) selected @endif value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Qty</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control"  value="{{$lineItem->qty}}" name="qty" required>
                            </div>
                        </div>


                        <input type="hidden" class="form-control" value="{{$lineItem->detail}}" name="detail">


                        <div class="form-group">
                            <label class="col-md-4 control-label">Note</label>

                            <div class="col-md-6">
                                <textarea name="note"  class="form-control">{{$lineItem->note}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection