@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit type product map
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('doEditType') }}">
                        {{ csrf_field() }}
                        <input name="type_id" type="hidden" value="{{ $types->id }}">
                        <div class="form-group{{ $errors->has('itGlueConfTypeName') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select ItGLue Config Type</label>
                            <input id="itGlueConfTypeName" name="itGlueConfTypeName" type="hidden" value="{{$types->itGlueConfTypeName}}" >
                            <div class="col-md-6">
                                <select class="form-control product" name="itGlueConfTypeId">
                                    <option data-value="{{$types->itGlueConfTypeName}}" value="{{$types->itGlueConfTypeId}}">{{$types->itGlueConfTypeName}}</option>
                                    @foreach( $itGlueTypes as $itGlueType )
                                        <option data-value="{{ $itGlueType->attributes->{'name'} }}" value="{{$itGlueType->id}}">{{ $itGlueType->attributes->{'name'}  }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('itGlueConfTypeName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('itGlueConfTypeName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Product</label>
                            <input id="product_name" name="product_name" type="hidden" value="{{$types->product_name}}" >
                            <div class="col-md-6">
                                <select class="form-control type" name="product_id">
                                    @foreach( $products as $key =>$product )
                                        <option @if($types->product_name == $product ) selected @endif data-value="{{$product}}" value="{{$key}}">{{$product}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('product_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>







                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection