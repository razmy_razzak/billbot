@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add new Line Item
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('createItemLine') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="configIds" value="0">

                        <div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Customer</label>

                            <div class="col-md-6">
                                <select class="form-control" name="customer_id">
                                    @foreach($customers as $key => $customer)
                                    <option value="{{$key}}">{{$customer}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Product</label>

                            <div class="col-md-6">
                                <select class="form-control" name="product_id">
                                    @foreach($products as $key => $product)
                                        <option value="{{$product->id}}">{{$product->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Qty</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="qty" required>
                                @if ($errors->has('qty'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--lazy to remove this filed so just hiding it--}}
                        <input type="hidden" class="form-control" name="detail" value="Non">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Note</label>

                            <div class="col-md-6">
                                <textarea name="note" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection