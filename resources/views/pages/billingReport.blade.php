@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Billing Report</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                {{--<div class="panel-heading">--}}
                    {{--Map product with item types--}}
                {{--</div>--}}
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @php($customerArray =[])

                    {{--<pre>{{var_dump($lastSyncedItems)}}</pre>--}}

                    <ul class="timeline">
                        @foreach( $lastSyncedItems as $lastSyncedItem)
                        <li>
                            {{--left column box--}}
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">{{$lastSyncedItem->customerName}}</h4>
                                </div>
                                <div class="timeline-body">
                                    @if(!$lastSyncedItem->pre_deploy_products && !$lastSyncedItem->active_products && !$lastSyncedItem->pre_deploy_products && !$lastSyncedItem->unClssified_prodcts )
                                        No Items or configuration added
                                        <form class="form-horizontal"  method="POST" action="{{ url('acceptDataDelete') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="customer_id" value="{{$lastSyncedItem->customerId}}">
                                            <button type="submit" class="btn btn-primary">
                                                Accept Data Into BillBot
                                            </button>
                                        </form>
                                    @endif
                                    <form class="form-horizontal" method="POST" action="{{ url('acceptData') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="customer_id" value="{{$lastSyncedItem->customerId}}">
                                    @include('parts.specialProduct' ,['specialProducts'=> $lastSyncedItem->active_products,'lastSyncedItem' =>$lastSyncedItem])
                                    @if($lastSyncedItem->pre_deploy_products)
                                    @include('parts.left_box_billreport' ,['config_items'=> $lastSyncedItem->pre_deploy_products, 'config_title'=> 'Pre-Deployed'])
                                    @endif
                                    @if($lastSyncedItem->unClssified_prodcts)
                                    @include('parts.left_box_billreport' ,['config_items'=> $lastSyncedItem->unClssified_prodcts, 'config_title'=> 'Unclassified'])
                                    @endif
                                        @if($lastSyncedItem->pre_deploy_products || $lastSyncedItem->active_products || $lastSyncedItem->pre_deploy_products || $lastSyncedItem->unClssified_prodcts )
                                         <button type="submit" class="btn btn-primary">
                                            Accept Data Into BillBot
                                        </button>
                                        @endif
                                    </form>
                                </div>

                                @inject('getPendingSummary', 'App\Http\Controllers\ProductController')
                                @inject('getLineItems', 'App\Http\Controllers\ProductController')
                                @php( $result = $getLineItems->getLineItemById($lastSyncedItem->customerId ) )
                                @php($customerArray[]= $result['lineItem'])
                                @php( $data = $getPendingSummary->getPendingSummary( $lastSyncedItem->active_products, $result['lineItem']  ) )

                                <div class="timeline-panel summary">
                                    <hr class="item_devider">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">Pending Summary</h4>
                                    </div>
                                    <div class="timeline-body">
                                        @foreach( $data as $key => $items)
                                            <div class="">
                                                <strong>{{$key}}</strong>
                                                    <strong class="floatRight"> $ {{$items['total']}} </strong>
                                                <hr class="item_line">
                                            </div>
                                            @foreach( $items['item'] as $item)
                                                @if(isset($item->configs))
                                                    <ul class="list-unstyled space_botom">
                                                        @foreach( $item->configs as $conf)
                                                            <li>{{$conf['name']}} <i class="fa {{$conf['icon']}} floatRight"></i></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                                @if(isset($item->icon))
                                                    <ul class="list-unstyled space_botom">
                                                        <li>{{$item->itGlueName}} <i class="fa {{$item->icon}} floatRight"></i></li>
                                                    </ul>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            {{--Right column box--}}

                            <div class="timeline-panel right_box">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">{{$lastSyncedItem->customerName}}</h4>
                                </div>
                                <div class="timeline-body">
                                    @if( $result )
                                        @include('parts.lineItem_box',['result'=>$result])
                                    @else
                                        @include('parts.no_box')
                                    @endif
                                </div>
                            </div>
                        </li>
                            <hr class="item_devider">
                        @endforeach
                    </ul>
                    {{ $pagination }}
                    <div class="shadowbox right_box">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Client-wide billing summary</h4>
                        </div>
                        <hr class="total_divder">
                        <hr class="total_divder">
                        <div class="timeline-body">
                            <ul class="list-unstyled space_botom">
                                @inject('clientTotal', 'App\Http\Controllers\ProductController')
                                @php($result = $clientTotal->clientWideTotal($customerArray))
                                <li>
                                    {{\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE}} Total: <span class="floatRight">$ {{$result[\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE]}}</span>
                                </li>
                                <li>
                                    {{\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE}} Total: <span class="floatRight">$ {{$result[\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE]}}</span>
                                </li>
                                <li>
                                    {{\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE}} Total: <span class="floatRight">$ {{$result[\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE]}}</span>
                                </li>
                            </ul>

                            <p>Subtotal: <span class="floatRight">$ {{$result[\App\Objects\ConstValues::CLIENT_WIDE_SUBTOTAL]}} </span></p>
                            <p>CBJ Sales tax: <span class="floatRight">$ {{$result[\App\Objects\ConstValues::CLIENT_WIDE_CBJ_TAX]}} </span></p>
                            <strong>Invoice Total: <strong class="floatRight">$ {{$result[\App\Objects\ConstValues::CLIENT_WIDE_INVOICE]}} </strong></strong>
                        </div>
                    </div>

                    <div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection