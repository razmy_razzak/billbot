@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Products and Services</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Set price for products and services
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <a target="_blank" href="{{url('productCreate')}}" class="btn btn-primary"> Add NEW</a>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Description</th>
                            <th>Product Type</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr class="odd gradeX">
                                <td>{{$product->name}} @if( $product->deletable == 1 )<i class="fa fa-warning"></i> @endif</td>
                                <td>{{$product->description}}</td>
                                <td>{{$product->product_type}}</td>
                                <td class="center">{{$product->price}}</td>
                                <td class="center">
                                    <a href="{{url('productEdit')}}/{{$product->id}}" class="btn btn-warning"> Edit</a>
                                    @if( $product->deletable == 0 )
                                        <a href="{{url('deleteProduct')}}/{{$product->id}}" class="btn btn-danger"> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection