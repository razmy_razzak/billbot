@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Line Items</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Add or Edit Lines
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <a target="_blank" href="{{url('showLineItemForm')}}" class="btn btn-primary"> Add NEW</a>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Customer</th>
                            <th>Product</th>
                            <th>qty</th>
                            <th>Note</th>
                            <th>Detail</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lineItems as $lineItems)

                            <tr class="odd gradeX">
                                <td>{{$lineItems->id}}</td>
                                <td>{{$lineItems->customer['itGlueName']}}</td>
                                <td>{{$lineItems->product['name']}}</td>
                                <td>{{$lineItems->qty}}</td>
                                <td>{{$lineItems->note}}</td>
                                @php($items = explode(',', $lineItems->detail))
                                <td>
                                    @foreach($items as $data)
                                        {{$data}}
                                    @endforeach
                                </td>
                                <td class="center">
                                    @if( $lineItems->product['product_type'] != \App\Objects\ConstValues::SPECIAL_PRODUCT )
                                    <a href="{{url('editLineItem')}}/{{$lineItems->id}}" class="btn btn-warning"> Edit</a>
                                    <a href="{{url('deleteLineItem')}}/{{$lineItems->id}}" class="btn btn-danger"> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection