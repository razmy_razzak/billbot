@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add new Product
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('doCreate') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="deletable" value="0">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea class="form-control" value="{{ old('description') }}" name="description"></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Select Product Type</label>

                            <div class="col-md-6">
                                <select class="form-control" name="product_type">
                                    <option value="{{\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE}}">{{\App\Objects\ConstValues::SUBSCRIPTION_PAYMENT_TYPE}}</option>
                                    <option value="{{\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE}}">{{\App\Objects\ConstValues::SERVICE_PAYMENT_TYPE}}</option>
                                    <option value="{{\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE}}">{{\App\Objects\ConstValues::RENTAL_PAYMENT_TYPE}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Price $</label>

                            <div class="col-md-6">
                                <input type="number" class="form-control" name="price" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection