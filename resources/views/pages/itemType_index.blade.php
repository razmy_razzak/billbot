@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Item Type map</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @include('success.success')
            <div class="panel panel-default">
                <div class="panel-heading">
                    Map product with item types
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <a target="_blank" href="{{url('typeCreate')}}" class="btn btn-primary"> Add NEW</a>
                    <a href="{{url('resync')}}" class="btn btn-warning"> Re-Sync</a>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Configuration type name</th>
                            <th>Configuration type Id</th>
                            <th>Product id</th>
                            <th>Product name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($itemTypes as $itemType)
                            <tr class="odd gradeX">
                                <td>{{$itemType->itGlueConfTypeName}}</td>
                                <td>{{$itemType->itGlueConfTypeId}}</td>
                                <td>{{$itemType->product_id}}</td>
                                <td>{{$itemType->product_name}}</td>
                                <td class="center">
                                    <a href="{{url('typeEdit')}}/{{$itemType->id}}" class="btn btn-warning"> Edit</a>
                                    <a href="{{url('typeDelete')}}/{{$itemType->id}}" class="btn btn-danger"> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@endsection