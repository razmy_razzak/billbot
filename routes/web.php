<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();



Route::group(['middleware' => ['auth']], function() {

    Route::get('/', function () {
        return view('master');
    })->name('home');

    Route::get('regi', 'RegisterContoller@showRegister');
    Route::get('adminEdit/{id}', 'RegisterContoller@showAdminEdit');

    Route::post('updateAdmin', 'RegisterContoller@updateAdmin');

    Route::get('productIndex', 'ProductController@index');
    Route::get('productCreate', 'ProductController@showForm');
    Route::get('productEdit/{id}', 'ProductController@showForm');
    Route::post('doCreate', 'ProductController@doCreate');
    Route::post('doEdit', 'ProductController@doEdit');
    Route::get('deleteProduct/{id}', 'ProductController@deleteProduct');

    Route::get('typeIndex', 'ProductController@showType');
    Route::get('typeCreate', 'ProductController@showTypeForm');
    Route::get('typeEdit/{id}', 'ProductController@showTypeForm');
    Route::get('typeDelete/{id}', 'ProductController@typeDelete');
    Route::post('doCreateType', 'ProductController@doCreateType');
    Route::post('doEditType', 'ProductController@doEditType');

    Route::get('resync', 'ProductController@reSyncConfigs');

    Route::get('syncItem', 'ProductController@doSyncItem');
    Route::get('billingReport', 'ProductController@showBillingReport');

    Route::get('resetLastUpdate', 'ProductController@resetLastUpdate');

    //line item
    Route::get('showLineItemForm', 'LineItemController@showLineItemForm');
    Route::get('editLineItem/{id}', 'LineItemController@showLineItemForm');
    Route::post('createItemLine', 'LineItemController@createLineItem');
    Route::get('lineItems', 'LineItemController@index');
    Route::post('updateLineItem', 'LineItemController@updateLineItem');
    Route::get('deleteLineItem/{id}', 'LineItemController@deleteLineItem');


    Route::post('acceptData', 'ProductController@acceptDataIntoBillBot');
    Route::post('acceptDataDelete', 'ProductController@deleteLineItem');


    //Settings
    Route::get('settings', 'SettingController@index');
    Route::get('showForm', 'SettingController@showSettingsForm');
    Route::get('showForm/{id}', 'SettingController@showSettingsForm');
    Route::post('createSetting', 'SettingController@createSetting');
    Route::post('editSetting', 'SettingController@editSetting');

});
